package com.example.eugn.isimplehomework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    private EditText mLoginView;
    private EditText mPasswordView;
    private View mProgressView;
    private Button mSignInButton;
    private BroadcastReceiver mBrReceiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set up the login form.
        mLoginView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mProgressView = findViewById(R.id.login_progress);
        mBrReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showProgress(false);
                int result = intent.getIntExtra(RestService.PARAM_RESULT, 0);
                if(result != 200){
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.
                      setTitle("Error").setPositiveButton("OK",null).
                      setMessage(intent.getStringExtra(RestService.PARAM_MESSAGE)+" "+result).show();
                }
                // получили ответ ОК, запускаем активность со счетами
                else startActivity(new Intent(MainActivity.this, AccountsActivity.class));
            }
        };
        registerReceiver(mBrReceiver, new IntentFilter(RestService.BROADCAST_ACTION));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBrReceiver);
    }


    private void attemptLogin() {
        // Reset errors.
        mLoginView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String login = mLoginView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)){
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if(!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid login.
        if (TextUtils.isEmpty(login)) {
            mLoginView.setError(getString(R.string.error_field_required));
            focusView = mLoginView;
            cancel = true;
        } else if (!isLoginValid(login)) {
            mLoginView.setError(getString(R.string.error_invalid_email));
            focusView = mLoginView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            showProgress(true);
            Intent startServiceIntent = new Intent(this, RestService.class);
            startServiceIntent.putExtra(UtilsAndConsts.TAG_LOGIN, login);
            startServiceIntent.putExtra(UtilsAndConsts.TAG_PASSWORD, password);
            startService(startServiceIntent);
        }


    }


    private boolean isLoginValid(String login) {
        return login.length() > 4;
    }


    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    private void showProgress(final boolean show) {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginView.setEnabled(!show);
            mPasswordView.setEnabled(!show);
            mSignInButton.setEnabled(!show);
    }
}


