package com.example.eugn.isimplehomework;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

//import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import android.util.Base64;


/**
 * Created by eugn on 12.02.2017.
 */

class iSimpleUtils {
    private iSimpleUtils(){}

    static String generateCnonce() {
        byte[] randomBytes = new byte[128];
        SecureRandom random = new SecureRandom();
        random.nextBytes(randomBytes);
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            return new String(Hex.encodeHex(digest.digest(randomBytes)));
        } catch (NoSuchAlgorithmException ignored) {
        }
        return null;
    }


    static String encodeA1(String username, String password, String realm, String... aux) {
        if (aux != null && aux.length == 2) {
            String salt = aux[0];
            String hashAlgorithm = aux[1] == null ? "md5" : aux[1];
            try {
                MessageDigest messageDigest = MessageDigest.getInstance(hashAlgorithm);
                String passwordAndSalt = salt.isEmpty() ?
                        password :
                        password + "{" + new String(Base64.decode(salt, Base64.DEFAULT)) + "}";
                if (passwordAndSalt != null) {
                    String passwordAndSaltDigest = new String(Hex.encodeHex(messageDigest.digest(passwordAndSalt.getBytes())));
                    messageDigest = MessageDigest.getInstance("md5");
                    String a1 = username + ":" + realm + ":" + passwordAndSaltDigest;
                    return new String(Hex.encodeHex(messageDigest.digest(a1.getBytes())));
                }
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }


    static String ncAsString(int nc) {
        String hex = Long.toHexString(nc);
        return "00000000".substring(hex.length()) + hex;
    }


    static String calculateResponse(String nonce, String cnonce, String uri,
                                    String method, String nc, String qop,
                                    String salt, String hashAlg, String userName,
                                    String passwd, String realm) {
        StringBuilder value = new StringBuilder(encodeA1(userName, passwd, realm, salt, hashAlg));
        value.append(":").append(nonce);
        value.append(":").append(nc);
        value.append(":").append(cnonce);
        value.append(":").append(qop);
        try {
            MessageDigest msgDigest = MessageDigest.getInstance("MD5");
            String a2 = method + ":" + uri;
            String encodedA2 = new String(Hex.encodeHex(msgDigest.digest(a2.getBytes())));
            value.append(":").append(encodedA2);
            msgDigest.reset();
            return new String(Hex.encodeHex(msgDigest.digest(value.toString().getBytes())));
        } catch (NoSuchAlgorithmException ignored) {
        }
        return null;
    }



}
