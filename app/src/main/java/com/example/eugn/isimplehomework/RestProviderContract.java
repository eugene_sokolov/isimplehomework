package com.example.eugn.isimplehomework;

import android.net.Uri;

/**
 * Created by eugn on 14.02.2017.
 */

class RestProviderContract {
    static final String ACCOUNT_TABLE = "account";
    static final String FIELD_ID = "_id";
    static final String FIELD_NUMBER = "number";
    static final String FIELD_CURRENCY = "currency";
    static final String FIELD_BALANCE = "balance";
    static final String FIELD_ALIAS = "alias";


    // // Uri
    // authority
    static final String AUTHORITY = "com.example.eugn.isimplehomework.RestProvider";
    // path
    static final String ACCOUNT_PATH = RestProviderContract.ACCOUNT_TABLE;
    // Общий Uri
    static final Uri ACCOUNT_CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/" + ACCOUNT_PATH);
}
