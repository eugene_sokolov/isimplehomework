package com.example.eugn.isimplehomework;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by eugn on 11.02.2017.
 */

abstract class HttpAsyncAccountsRequest extends AsyncTask<String, String, String> {
    private final static String TAG = HttpAsyncAccountsRequest.class.getSimpleName();
    private Context mContext;
    private String mHost;
    private String mPathInit;
    private String mLogin;
    private String mPassword;
    private int mResultCode = 0;

    HttpAsyncAccountsRequest(Context context, String host, String path,
                             String login, String password) {
        mContext = context;
        mHost = host;
        mPathInit = path;
        mLogin = login;
        mPassword = password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public abstract void onAnswer(int code, String message);

    @Override
    protected String doInBackground(String... nothing) {
        try {
            // первый шаг авторизации ---------------------------------------------------
            HttpSimpleRequest req = new HttpSimpleRequest(mHost+mPathInit);
            req.addRequestProperty("Authorization", "SP Digest username=\""+mLogin+"\"");
            HttpSimpleRequest.Response response = req.getResponse();
            String nonce = null;
            String salt = null;
            String qop = null;
            String hashalg = null;
            String cnonce = null;
            String responsestr = null;
            final String realm = "SD Digest Authentication Realm";

            mResultCode = response.code;
            if (mResultCode == 401){
                String auth  = response.getHeaderParam("WWW-Authenticate");
                if(auth.startsWith("SP Digest")){
                    nonce = UtilsAndConsts.getNamedTokenFromString(auth, "nonce");
                    salt = UtilsAndConsts.getNamedTokenFromString(auth, "salt");
                    qop = UtilsAndConsts.getNamedTokenFromString(auth, "qop");
                    hashalg = UtilsAndConsts.getNamedTokenFromString(auth, "hashalg");
                    cnonce = iSimpleUtils.generateCnonce();
                    responsestr = iSimpleUtils.calculateResponse(nonce, cnonce,
                            mHost+mPathInit, "GET", iSimpleUtils.ncAsString(1),
                            qop, salt, hashalg, mLogin, mPassword, realm);
                    Log.d(TAG, "nonce = " + nonce);
                    Log.d(TAG, "salt = " + salt);
                    Log.d(TAG, "qop = " + qop);
                    Log.d(TAG, "hashalg = " + hashalg);
                    Log.d(TAG, "cnonce = " + cnonce);
                    Log.d(TAG, "response = " + responsestr);
                } else throw new IOException("Error: response.header doesn't" +
                        " starts with \"SP Digest\"");
            } else throw new IOException(response.message);

            // второй шаг авторизации ---------------------------------------------------
            if(nonce != null && salt != null && qop != null &&
               hashalg != null && cnonce != null && responsestr != null) {

                req = new HttpSimpleRequest(mHost + mPathInit);
                String auth = String.format("SP Digest username=\"%s\", realm=\"%s\", " +
                                "nonce=\"%s\", uri=\"%s\", " +
                                "response=\"%s\", qop=\"%s\", nc=\"%s\", cnonce=\"%s\"",
                                    mLogin,
                                    realm,
                                    nonce,
                                    mHost + mPathInit,
                                    responsestr,
                                    qop,
                                    iSimpleUtils.ncAsString(1),
                                    cnonce);
                req.addRequestProperty("Authorization", auth);

                response = req.getResponse();
                mResultCode = response.code;
                if(mResultCode == 401){
                    String auth_step2 = response.getHeaderParam("WWW-Authenticate");
                    //TODO обработать ошибки в заголовке (заблокирован?, надолго?)
                    throw new IOException(response.message);
                } else
                if(mResultCode == 200) {
                    // авторизация завершена ----------------------------------------------
                    parseJSON(response.in);
                }
            }
            return ""; // TODO что ответить, если все ОК

        } catch (Exception e) {
            String error = e.getMessage();
            System.out.println(error);
            return error;
        }
    }

    private void parseJSON(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {

            int deleted = mContext.getContentResolver().delete(RestProviderContract.ACCOUNT_CONTENT_URI, null, null);
            Log.d(TAG, "Deleted "+deleted+" records");

            reader.beginObject();
            final String arrName = reader.nextName();
            if (arrName.equals("account")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    reader.beginObject();
                    Integer id = null;
                    String number = null;
                    String currency = null;
                    Double balance = null;
                    String alias = null;
                    while (reader.hasNext()) {
                        String objName = reader.nextName();
                        if(objName.equals("bankingInformation")) {
                            reader.skipValue(); // read bankingInformation
                        }else if(objName.equals("id")){
                            id = reader.nextInt();
                        }else if(objName.equals("number")){
                            number = reader.nextString();
                        }else if(objName.equals("currency")) {
                            currency = reader.nextString();
                        }else if(objName.equals("balance")){
                            balance = reader.nextDouble();
                        }else if(objName.equals("alias")){
                            alias = reader.nextString();
                        }else reader.skipValue();
                    }
                    reader.endObject();



                    ContentValues cv = new ContentValues();
                    cv.put(RestProviderContract.FIELD_ID, id);
                    cv.put(RestProviderContract.FIELD_ALIAS, alias);
                    cv.put(RestProviderContract.FIELD_CURRENCY, currency);
                    cv.put(RestProviderContract.FIELD_BALANCE, balance);
                    cv.put(RestProviderContract.FIELD_NUMBER, number);

                    Uri newUri =  mContext.getContentResolver().insert(RestProviderContract.ACCOUNT_CONTENT_URI, cv);
                    Log.d(TAG, "insert, result Uri : " + newUri.toString());



                    Log.d(TAG, String.format("Account id=%d, number=%s, currcode=%s,"+
                                    " balance=%f, alias=%s",
                            id, number, currency, balance, alias));
                }
                reader.endArray();
            }
            else Log.d(TAG, "Error: No \"account\" object in JSON stream");
            reader.endObject();
        }
        finally {
            reader.close();
        }
    }

    @Override
    protected void onPostExecute(String message) {
        onAnswer(mResultCode, message);
    }




}
