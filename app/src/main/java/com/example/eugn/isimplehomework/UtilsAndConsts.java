package com.example.eugn.isimplehomework;

import android.content.res.Resources;

import java.io.FileNotFoundException;
import java.util.NoSuchElementException;

/**
 * Created by eugn on 11.02.2017.
 */

class UtilsAndConsts {
    private UtilsAndConsts(){}

    static final String TAG_LOGIN = BuildConfig.APPLICATION_ID+"_LOGIN";
    static final String TAG_PASSWORD = BuildConfig.APPLICATION_ID+"_PASSWORD";


    /**
     * Функция для получения параметров из строки вида парам=значение парам=значение
     * @param taggedString
     * @param tokenName
     * @return
     * @throws NoSuchElementException
     */

    static String getNamedTokenFromString(String taggedString, String tokenName)
                                                                throws NoSuchElementException {
        int start = taggedString.indexOf(tokenName+"=\"");
        if(start != -1){
            start += tokenName.length()+2;
            return taggedString.substring(start,taggedString.indexOf("\"",start));
        }
        else throw new NoSuchElementException("Token \""+tokenName+"\" not found");
    }
}
