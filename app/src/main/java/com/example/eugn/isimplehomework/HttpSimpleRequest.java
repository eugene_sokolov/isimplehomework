package com.example.eugn.isimplehomework;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by eugn on 12.02.2017.
 */

class HttpSimpleRequest {
    private final static String TAG = HttpSimpleRequest.class.getSimpleName();
    private HttpURLConnection mCon;

    HttpSimpleRequest(String url) throws IOException {
        URL mUrl = new URL(url);
        mCon = (HttpURLConnection) mUrl.openConnection();
        Log.d(TAG,"====== NEW HttpURLConnection, URL: "+url);
    }

    HttpSimpleRequest addRequestProperty(String key, String property){
        Log.d(TAG,key+": "+property);
        mCon.addRequestProperty(key, property);
        return this;
    }

    HttpSimpleRequest.Response getResponse() throws IOException {
         /*
        try {
            resultToDisplay = IOUtils.toString(in, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        int code = mCon.getResponseCode();
        String message = mCon.getResponseMessage();
        Map<String, List<String>> header = mCon.getHeaderFields();

        InputStream in = null;
        try {
            in = mCon.getInputStream();
        }catch (IOException e){
            Log.d(TAG,"No input stream");
        }

        Log.d(TAG, "Response code: "+code);
        Log.d(TAG, "Response msg: "+message);

        Set<String> keys = header.keySet();
        for(String key : keys){
            List<String> list = header.get(key);
            for(String str : list){
                Log.d(TAG, key+": "+str);
            }
        }
        return new Response(code, message, header, in);
    }



    class Response{
        //TODO написать сеттеры и геттеры
        int code;
        String message;
        Map<String, List<String>> header;
        InputStream in;
        Response(int code, String message, Map<String, List<String>> header, InputStream in){
            this.code = code;
            this.message = message;
            this.header = header;
            this.in = in;
        }

        String getHeaderParam(String name){
            Set<String> keys = header.keySet();
            for(String key : keys){
                if(name.equals(key)) {
                    List<String> list = header.get(key);
                    String result = "";
                    for (String str : list) {
                        result = result+str;
                    }
                    return result;
                }
            }
            return null;
        }
    }
}
