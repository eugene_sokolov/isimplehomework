package com.example.eugn.isimplehomework;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class AccountsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ListView lv = (ListView) findViewById(R.id.listView);

        mAdapter = new MyAdapter(this, null, true);
        lv.setAdapter(mAdapter);
        getSupportLoaderManager().initLoader(0, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,
                RestProviderContract.ACCOUNT_CONTENT_URI, //uri для таблицы Classes
                null, //список столбцов, которые должны присутствовать в выборке
                null, // условие WHERE для выборки
                null, // аргументы для условия WHERE
                null); // порядок сортировки
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }


    class MyAdapter extends CursorAdapter{
        private LayoutInflater mInflater; //нужен для создания объектов класса View

        public MyAdapter(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View root = mInflater.inflate(R.layout.list_view_item, parent, false);
            ViewHolder holder = new ViewHolder();
            TextView tvNumber = (TextView)root.findViewById(R.id.accountNumber);
            TextView tvCurrency = (TextView)root.findViewById(R.id.accountCurrency);
            TextView tvBalance = (TextView)root.findViewById(R.id.accountBalance);
            holder.tvNumber = tvNumber;
            holder.tvCurrency = tvCurrency;
            holder.tvBalance = tvBalance;
            root.setTag(holder);
            return root;
        }

        @Override
        public void bindView(View view, Context context, Cursor cur) {
            long id = cur.getLong(cur.getColumnIndex(RestProviderContract.FIELD_ID));
            String number = cur.getString(cur.getColumnIndex(RestProviderContract.FIELD_NUMBER));
            String currency = cur.getString(cur.getColumnIndex(RestProviderContract.FIELD_CURRENCY));
            String balance = String.valueOf(cur.getDouble(cur.getColumnIndex(RestProviderContract.FIELD_BALANCE)));
            ViewHolder holder = (ViewHolder) view.getTag();
            if(holder != null) {
                holder.tvNumber.setText(number);
                holder.tvCurrency.setText(currency);
                holder.tvBalance.setText(balance);
            }

        }
    }

    public static class ViewHolder {
        TextView tvNumber;
        TextView tvCurrency;
        TextView tvBalance;
    }
}
