package com.example.eugn.isimplehomework;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

public class RestProvider extends ContentProvider {
    private static final String TAG = RestProvider.class.getSimpleName();

    static final String DB_NAME = "database";
    static final int DB_VERSION = 1;




    // Скрипт создания таблицы
    static final String DB_CREATE =
            String.format("create table %s (%s integer primary key, %s text, %s text, %s text, %s double)",
                    RestProviderContract.ACCOUNT_TABLE,
                    RestProviderContract.FIELD_ID,
                    RestProviderContract.FIELD_NUMBER,
                    RestProviderContract.FIELD_ALIAS,
                    RestProviderContract.FIELD_CURRENCY,
                    RestProviderContract.FIELD_BALANCE);



    // Типы данных
    // набор строк
    static final String ACCOUNT_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + RestProviderContract.AUTHORITY + "." + RestProviderContract.ACCOUNT_PATH;

    // одна строка
    static final String ACCOUNT_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + RestProviderContract.AUTHORITY + "." + RestProviderContract.ACCOUNT_PATH;

    //// UriMatcher
    // общий Uri
    static final int URI_ACCOUNT = 1;

    // Uri с указанным ID
    static final int URI_ACCOUNT_ID = 2;

    // описание и создание UriMatcher

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(RestProviderContract.AUTHORITY, RestProviderContract.ACCOUNT_PATH, URI_ACCOUNT);
        uriMatcher.addURI(RestProviderContract.AUTHORITY, RestProviderContract.ACCOUNT_PATH + "/#", URI_ACCOUNT_ID);
    }

    DBHelper dbHelper;
    SQLiteDatabase db;


    public RestProvider() {
    }


    public String getType(@NonNull Uri uri) {
        Log.d(TAG, "getType, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ACCOUNT:
                return ACCOUNT_CONTENT_TYPE;
            case URI_ACCOUNT_ID:
                return ACCOUNT_CONTENT_ITEM_TYPE;
        }
        return null;
    }


    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG, "delete, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ACCOUNT:
                Log.d(TAG, "URI_CONTACTS");
                break;
            case URI_ACCOUNT_ID:
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_CONTACTS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = RestProviderContract.FIELD_ID + " = " + id;
                } else {
                    selection = selection + " AND " + RestProviderContract.FIELD_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.delete(RestProviderContract.ACCOUNT_TABLE, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }


    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Log.d(TAG, "insert, " + uri.toString());
        if (uriMatcher.match(uri) != URI_ACCOUNT)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        db = dbHelper.getWritableDatabase();
        long rowID = db.insert(RestProviderContract.ACCOUNT_TABLE, null, values);
        Uri resultUri = ContentUris.withAppendedId(RestProviderContract.ACCOUNT_CONTENT_URI, rowID);
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }


    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate");
        dbHelper = new DBHelper(getContext());
        return true;
    }


    // чтение
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query, " + uri.toString());
        // проверяем Uri
        switch (uriMatcher.match(uri)) {
            case URI_ACCOUNT: // общий Uri
                Log.d(TAG, "URI_ACCOUNT");
                // если сортировка не указана, ставим свою - по имени
                if (TextUtils.isEmpty(sortOrder))
                    sortOrder = RestProviderContract.FIELD_ALIAS + " ASC";
                break;
            case URI_ACCOUNT_ID: // Uri с ID
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_ACCOUNT_ID, " + id);
                // добавляем ID к условию выборки
                if (TextUtils.isEmpty(selection)) selection = RestProviderContract.FIELD_ID + " = " + id;
                else selection = selection + " AND " + RestProviderContract.FIELD_ID + " = " + id;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(RestProviderContract.ACCOUNT_TABLE, projection, selection,
                                    selectionArgs, null, null, sortOrder);
        // просим ContentResolver уведомлять этот курсор
        // об изменениях данных в ACCOUNT_CONTENT_URI
        cursor.setNotificationUri(getContext().getContentResolver(), RestProviderContract.ACCOUNT_CONTENT_URI);
        return cursor;
    }


    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        Log.d(TAG, "update, " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ACCOUNT:
                Log.d(TAG, "URI_CONTACTS");

                break;
            case URI_ACCOUNT_ID:
                String id = uri.getLastPathSegment();
                Log.d(TAG, "URI_CONTACTS_ID, " + id);
                if (TextUtils.isEmpty(selection)) {
                    selection = RestProviderContract.FIELD_ID + " = " + id;
                } else {
                    selection = selection + " AND " + RestProviderContract.FIELD_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.update(RestProviderContract.ACCOUNT_TABLE, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }





    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
            /*ContentValues cv = new ContentValues();
            for (int i = 1; i <= 3; i++) {
                cv.put(CONTACT_NAME, "name " + i);
                cv.put(CONTACT_EMAIL, "email " + i);
                db.insert(CONTACT_TABLE, null, cv);
            }*/
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
