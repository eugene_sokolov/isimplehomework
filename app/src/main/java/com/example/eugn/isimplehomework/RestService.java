package com.example.eugn.isimplehomework;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class RestService extends Service {

    private final static String TAG = RestService.class.getSimpleName();
    static final String BROADCAST_ACTION = TAG + "_BROADCAST";
    static final String PARAM_RESULT = TAG + "_PARAM_RESULT";
    static final String PARAM_MESSAGE = TAG + "_PARAM_MESSAGE";


    public RestService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        doRequest(  intent.getStringExtra(UtilsAndConsts.TAG_LOGIN),
                    intent.getStringExtra(UtilsAndConsts.TAG_PASSWORD));

        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return null;
    }


    private void doRequest(final String login, final String password){
        Log.d(TAG, "Request login:"+login+" password:"+password);
        final String host = "http://5.9.139.4:7777";
        final String path = "/rest/personal/account?sync_accounts=true";
        HttpAsyncAccountsRequest r = new HttpAsyncAccountsRequest(getApplicationContext(),
                                                                host, path, login, password) {
            @Override
            public void onAnswer(int code, String message) {
                Log.d(TAG, "Answer :"+message);
                sendBroadcast(new Intent(BROADCAST_ACTION).
                        putExtra(PARAM_RESULT, code).
                        putExtra(PARAM_MESSAGE, message));
                stopSelf();
            }
        };
        r.execute();


    }
}
